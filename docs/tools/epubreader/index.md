---
title: "Epub在线阅读器"
slug: "Epubreader"
date: 2022-03-07T14:40:42+08:00
draft: false
description: "自建&DEMO"
categories: [""]
tags: ["实用工具"]
---

Bibi是一个能够在浏览器中阅读EPUB文件的小工具。

[GitHub项目地址](https://redirect.1kbtool.com/?url=https://github.com/satorumurmur/bibi)

[下载地址](https://redirect.1kbtool.com/?url=https://github.com/satorumurmur/bibi/releases/download/v1.2.0/Bibi-v1.2.0.zip)

下载解压后打开`index.html`，会弹出浏览器界面，将想要阅读的文档拖进去即可。

由于本身也只是一个简单的静态文件，就找了个地方托管了一下，大家如果懒得下也可以用这个在线DEMO

[在线DEMO](https://bibi-demo.vercel.app/)

![](2022-03-07-14-47-14.png)

实际上，GitHub还有许多类似的EPUB JS工具。不过都没有像这个一样封装的这么好。可以查看文末的参考链接。

参考

1. https://github.com/futurepress/epub.js  epubjs：用于读取epub文件的js库。
