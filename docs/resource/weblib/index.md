---
title: "互联网图书馆：一个储存近20年绝大部分网页历史版本的海量信息在吸纳图书馆，被称作互联网“时光机”"
date: 2022-02-16T16:10:34+08:00
draft: false
slug: Weblib
categories: ["网站分享"]
tags: ["网页存档"]
---

## 简介


互联网档案馆（网址`web.archive.org`），是一个公益项目，专门收集互联网上的各种信息，例如视频、音频、网页等等并存储在他家的服务器中。
 
**互联网档案馆**（英语：Internet Archive）是[美国](https://zh.wikipedia.org/wiki/美国)的一个由[Alexa](https://zh.wikipedia.org/wiki/Alexa_Internet)创始人布鲁斯特·卡利创办于1996年的[非营利](https://zh.wikipedia.org/wiki/非營利)性的、提供[互联网](https://zh.wikipedia.org/wiki/互联网)[多媒体](https://zh.wikipedia.org/wiki/多媒体)[资料](https://zh.wikipedia.org/wiki/资料)[文件](https://zh.wikipedia.org/wiki/档案)阅览[服务](https://zh.wikipedia.org/wiki/服务)的[数字图书馆](https://zh.wikipedia.org/wiki/數位圖書館)，总部位于[加利福尼亚州](https://zh.wikipedia.org/wiki/加利福尼亚州)[旧金山](https://zh.wikipedia.org/wiki/旧金山)的列治文区，其使命是“普及所有知识”（英语：universal access to all knowledge.）[[注 1\]](https://zh.wikipedia.org/zh-cn/互联网档案馆#cite_note-5)[[注 2\]](https://zh.wikipedia.org/zh-cn/互联网档案馆#cite_note-6)。该“[档案馆](https://zh.wikipedia.org/w/index.php?title=档案馆&action=edit&redlink=1)”提供的数字资料有如网站、网页、图形材料音乐、视频、音频、软件、动态图像和数百万书籍等的永久性免费储存及获取的副本。 迄至2012年10月，其信息储量达到10[PB](https://zh.wikipedia.org/wiki/拍字节)（即10,240[TB](https://zh.wikipedia.org/wiki/太字节)）[[5\]](https://zh.wikipedia.org/zh-cn/互联网档案馆#cite_note-7)[[6\]](https://zh.wikipedia.org/zh-cn/互联网档案馆#cite_note-8)。除此之外，该档案馆也是网络开放与自由化的倡议者之一。

其中值得关注的就是他们的网页备份项目。这个项目已经做到了对于非常多的网页，对于每一个版本都留下了历史记录。即通过这个项目可以访问**大多数网页的任意时间节点的版本，即使这个网站本身已经关停**

以百度为例，我可以选择20年前、10年前的版本进行查看，可以看到除了Flash由于已经被淘汰不能播放之外，其他内容都很清晰。

链接地址：[百度在线网络技术(北京)有限公司 (archive.org)](https://web.archive.org/web/20010301195557/http://www.baidu.com/)

![image-20211126195029500](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211126195029500.png)

![image-20211126195401096](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211126195401096.png)

大家也可以用这个项目看看阿里、腾讯等网站以前的样子。只需要在最上方地址处进行修改，然后左右箭头条件时间就可以了。

附注：

由于这个项目的体量非常庞大，在其中检索访问数据时会比较慢，也是可以理解的。



参考：
