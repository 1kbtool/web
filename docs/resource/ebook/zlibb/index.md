---
title: "Z-library 镜像搭建"
description: "Z-library 镜像搭建"
slug: Zlibb
date: 2022-08-14T09:18:24+08:00
draft: false
tags: ['Z-Library','电子书','镜像网站']
categories: ['']
---

# 使用 Cloudflare 构建 Zlibrary 镜像站

本部分介绍使用 Cloudflare （一家国外的云服务商）的免费服务构建 Zlibrary 的镜像站。虽然服务是免费的，但是 Cloudflare 提供的域名已经被墙了。所以我们需要自备一个域名。

## 准备 Cloudflare 服务

转到：[Cloudflare - The Web Performance Security Company](https://redirect.1kbtool.com/?url=https://www.cloudflare.com/) 注册一个账号。

从官网注册之后，会跳转到转到 [控制台](https://redirect.1kbtool.com/?url=https://dash.cloudflare.com/)，接下来的步骤都是在控制台进行的。右上角可以切换中英文。

在控制面板主页左侧可以找到`workers`。如图所示，中间可以创建服务，右侧显示每天的额度，如果只是搭建个人服务这些额度绰绰有余了（但是放在网上公开使用大概率会用完，这也是我推荐大家自己搭建的原因）。下方会显示所有已经搭建的服务。

![img](2022-07-19-14-46-21.png)

创建服务，随便填一个名称即可。

![img](2022-07-19-14-47-01.png)

等待部署完成，回到 [控制台](https://redirect.1kbtool.com/?url=https://dash.cloudflare.com/)，workers-点击刚刚创建的项目。转到控制界面，可以找到右下角有一个快速编辑的按钮。

![img](2022-07-19-14-48-07.png)

打开这个窗口，把代码以下复制进去，保存部署。注意一定要复制全，并且不要改动！已经配置好了。电脑可以按住左键然后滑动滚轮连续复制。

![img](2022-07-19-14-48-14.png)

```js
// 你要镜像的网站.
const upstream = 'zh.1lib.education'

// 镜像网站的目录，比如你想镜像某个网站的二级目录则填写二级目录的目录名，镜像 google 用不到，默认即可.
const upstream_path = '/'

// 镜像站是否有手机访问专用网址，没有则填一样的.
const upstream_mobile = 'zh.1lib.education'

// 屏蔽国家和地区.
const blocked_region = ['KP', 'SY', 'PK', 'CU']

// 屏蔽 IP 地址.
const blocked_ip_address = ['0.0.0.0', '127.0.0.1']

// 镜像站是否开启 HTTPS.
const https = true

// 文本替换.
const replace_dict = {
    '$upstream': '$custom_domain',
}

// 以下保持默认，不要动
addEventListener('fetch', event => {
    event.respondWith(fetchAndApply(event.request));
})

async function fetchAndApply(request) {

    const region = request.headers.get('cf-ipcountry').toUpperCase();
    const ip_address = request.headers.get('cf-connecting-ip');
    const user_agent = request.headers.get('user-agent');

    let response = null;
    let url = new URL(request.url);
    let url_hostname = url.hostname;

    if (https == true) {
        url.protocol = 'https:';
    } else {
        url.protocol = 'http:';
    }

    if (await device_status(user_agent)) {
        var upstream_domain = upstream;
    } else {
        var upstream_domain = upstream_mobile;
    }

    url.host = upstream_domain;
    if (url.pathname == '/') {
        url.pathname = upstream_path;
    } else {
        url.pathname = upstream_path + url.pathname;
    }

    if (blocked_region.includes(region)) {
        response = new Response('Access denied: WorkersProxy is not available in your region yet.', {
            status: 403
        });
    } else if (blocked_ip_address.includes(ip_address)) {
        response = new Response('Access denied: Your IP address is blocked by WorkersProxy.', {
            status: 403
        });
    } else {
        let method = request.method;
        let request_headers = request.headers;
        let new_request_headers = new Headers(request_headers);

        new_request_headers.set('Host', url.hostname);
        new_request_headers.set('Referer', url.hostname);

        let original_response = await fetch(url.href, {
            method: method,
            headers: new_request_headers
        })

        let original_response_clone = original_response.clone();
        let original_text = null;
        let response_headers = original_response.headers;
        let new_response_headers = new Headers(response_headers);
        let status = original_response.status;

        new_response_headers.set('access-control-allow-origin', '*');
        new_response_headers.set('access-control-allow-credentials', true);
        new_response_headers.delete('content-security-policy');
        new_response_headers.delete('content-security-policy-report-only');
        new_response_headers.delete('clear-site-data');

        const content_type = new_response_headers.get('content-type');
        if (content_type.includes('text/html') && content_type.includes('UTF-8')) {
            original_text = await replace_response_text(original_response_clone, upstream_domain, url_hostname);
        } else {
            original_text = original_response_clone.body
        }

        response = new Response(original_text, {
            status,
            headers: new_response_headers
        })
    }
    return response;
}

async function replace_response_text(response, upstream_domain, host_name) {
    let text = await response.text()

    var i, j;
    for (i in replace_dict) {
        j = replace_dict[i]
        if (i == '$upstream') {
            i = upstream_domain
        } else if (i == '$custom_domain') {
            i = host_name
        }

        if (j == '$upstream') {
            j = upstream_domain
        } else if (j == '$custom_domain') {
            j = host_name
        }

        let re = new RegExp(i, 'g')
        text = text.replace(re, j);
    }
    return text;
}


async function device_status(user_agent_info) {
    var agents = ["Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"];
    var flag = true;
    for (var v = 0; v < agents.length; v++) {
        if (user_agent_info.indexOf(agents[v]) > 0) {
            flag = false;
            break;
        }
    }
    return flag;
}
```

## 配置域名

本来到上面这一步，直接打开镜像即可。但由于被国内用户滥用，防火墙已经精准识别 Cloudflare 域名了。这里只能重新弄一个域名。

域名注册最方便的肯定是国内的 [DNSPOD](https://redirect.1kbtool.com/?url=https://buy.cloud.tencent.com/domain?intl=0) 这种。。但这里不太推荐因为这个事情不是特别干净。国内注册的方法是一样的。

这里用国外的服务 [Name.com](https://redirect.1kbtool.com/?url=https://www.name.com/referral/476d17) 注册 XYZ 域名。

首次注册可以获得 5 美元优惠券，但是需要 5.01 美元才能使用。用处不大。另外这个活动似乎有时候失效有时候有效。

![img](2022-07-19-15-14-24.png)

如图，随便搜一个 6 位的 xyz 后缀的纯数字域名，价格就变成 0.99 刀了。

![img](2022-07-19-15-14-50.png)

点击加购物车，点checkout，跳到广告页面，拉到最下面，继续付款：

![img](2022-07-19-15-15-55.png)

域名是按年买的。这里买一年就行。注意删掉高级保护。

![img](2022-07-19-15-16-37.png)

付款，这样你就获得了一个域名。我们需要把域名解析到 Cloudflare （让 Cloudflare 知道你买了个域名）。先回到 [控制台](https://redirect.1kbtool.com/?url=https://dash.cloudflare.com/)，选择添加：

![img](2022-07-19-15-23-40.png)

**输入你刚买的 XYZ 域名，点 Add Site，拉到下面选择 Free $0 （免费计划），点 Coutinue，扫描 DNS 记录（这个不用管），直接点 Continue**，然后可以看到：

![img](2022-07-19-15-25-46.png)

复制下面 Click to copy 的内容，两个都要复制，之后要用。

转到 [域名管理](https://redirect.1kbtool.com/?url=https://www.name.com/account/domain)

![img](2022-07-19-15-19-22.png)

如图，我们要把这个 NameServer 改成 Cloudflare 提供给我们的。每一个人的 NameServer 都不一样！不要照抄，否则你的域名就放到我账号下了。

![img](2022-07-19-15-21-34.png)

再回到 [控制台](https://redirect.1kbtool.com/?url=https://dash.cloudflare.com/)，workers-点击刚刚创建的项目，点击 Triggers，如图输入，然后划到下面点蓝色的 Add Custom Domain。这一步是把域名绑定到我们搭建的镜像网站。

![img](2022-07-19-15-33-59.png)

**Congratulations！接下来访问你的域名即可**

如果你还想用那个优惠券（并且优惠券可以用的话），转到 [域名管理](https://redirect.1kbtool.com/?url=https://www.name.com/account/domain) 选择续费RENEW DOMAIN，下方点击CHECK OUT，选择6年（5年续费价格不到5.01美元，用不了优惠），结账，不要忘记选上优惠码。可以用支付宝。

![img](2022-07-19-15-16-50.png)

## 附录

### Reference

- [Document (sosoushu.com)](https://redirect.1kbtool.com/?url=https://doc.sosoushu.com/#/)

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

