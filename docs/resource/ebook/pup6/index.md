---
title: "北大出版社电子教材书架：在线阅读本社电子书"
description: "北大出版社电子教材书架"
slug: Pup6
date: 2022-08-09T22:45:05+08:00
draft: false
tags: ['电子书']
categories: ['']
---

## 简介

北大出版社电子教材书架支持在线阅读本社电子书。

目前网站处于半瘫痪状态，但仍然有一些土木工程方面的书籍可以在线全本阅读。另外有一些书目可以试读。

主要是课本教材。

![](./2022-08-09-22-49-42.png)


### 相关链接

- [北大出版社电子教材书架](https://redirect.1kbtool.com/?url=https://pup6.yunzhan365.com/bookcase/kiru/index.html)

## 使用教程

按书目浏览即可。放大可以查看更清晰版本。

对书目的保存备份工作正在进行中。

<!-- ID 为 zpcv
<img class="thumbImg" style="pointer-events: none; max-width: 86px; max-height: 116px; width: 82.5257px; height: 116px;" src="https://book.yunzhan365.com/kbms/zpcv/files/shot.jpg?1640652912000">
获取图片，改数字即可
https://book.yunzhan365.com/kbms/bmuj/files/mobile/2.jpg?x-oss-process=image/format,webp -->


## 附录


### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

