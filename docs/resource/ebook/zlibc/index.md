---
title: "Z-library 可用网站检测"
description: "Zlibc"
slug: Zlibc
date: 2022-08-11T08:50:30+08:00
draft: false
tags: ['Z-Library','电子书']
categories: ['']
---

## 简介

Z-library 可用站点检测小工具，基于 JS 技术，在您的浏览器中实时运行检测服务，一次性检测多个网址。

绿色的即可以访问。（仅仅表示可以打开，不一定可以搜书）。更多内容请参考网站介绍。

![](./2022-08-11-09-00-05.png)

### 相关链接

- [Zlibrary 可用检测](https://check.1kbtool.com/zlibrary/)

## 使用教程

如上，直接访问即可。

## 附录

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

