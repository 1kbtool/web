---
title: "Z-Library 登录教程 2022/8/16"
description: "Z-Library 登录教程"
slug: Zlibsign
date: 2022-08-16T00:18:05+08:00
draft: true
tags: ['Z-Library','电子书']
categories: ['']
---

## 简介

最近 Zlibrary 的官方站点已经墙的一个都不剩了。这里只能考虑通过一些比较硬核的方法注册登录。

### 相关链接

## 使用教程

### 注册

首先打开：[Online Python Compiler (Interpreter)](https://www.programiz.com/python-programming/online-compiler/)

复制并粘贴以下代码：

```py
import requests

cookies = {
    'domains-availability': '%7B%22books%22%3A%22zh.b-ok.global%22%2C%22articles%22%3A%22zh.booksc.org%22%2C%22redirector%22%3A%22zh.1lib.domains%22%2C%22singlelogin%22%3A%22zh.singlelogin.app%22%7D',
}

headers = {
    'authority': 'zh.b-ok.global',
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    # 'cookie': 'domains-availability=%7B%22books%22%3A%22zh.b-ok.global%22%2C%22articles%22%3A%22zh.booksc.org%22%2C%22redirector%22%3A%22zh.1lib.domains%22%2C%22singlelogin%22%3A%22zh.singlelogin.app%22%7D',
    'origin': 'https://zh.b-ok.global',
    'referer': 'https://zh.b-ok.global/howtodonate.php',
    'sec-ch-ua': '"Chromium";v="104", " Not A;Brand";v="99", "Microsoft Edge";v="104"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36 Edg/104.0.1293.54',
    'x-requested-with': 'XMLHttpRequest',
}

data = {
    'isModal': 'true',
    # 此处请改成你的邮箱！！！！！！！！！
    'email': '1kbtool3@kermsite.com',
    # 此处请改成你的密码！！！！！！！！！
    'password': '123456789',
    # 此处请改成你的用户名！！！！！！！！！
    'name': '1kbtool3',
    'rx': '215',
    'action': 'registration',
    'gg_json_mode': '1',
}

print(requests.post('https://zh.b-ok.global/rpc.php', cookies=cookies, headers=headers, data=data).text)
```

如图，点击 run

![](./2022-08-16-00-25-07.png)

稍等一会。正常情况下可以得到如下结果：

```
{"errors":[],"response":{"redirectFromModal":"\/profile.php?remix_userkey=e2782876f30718f3e911eef474d18410&remix_userid=26262731"}}
```

![](./2022-08-16-00-26-42.png)

复制其中的

```
remix_userid=26262731
remix_userkey=e2782876f30718f3e911eef474d18410
```

注意不要把？和&复制进去了

然后打开镜像站，按F12键打开开发工具。复制以下命令，回车运行。先ID再KEY。

```
document.cookie='这里填你刚才复制的东西，分开填，运行两次'
```

![](./2022-08-16-00-29-48.png)

然后按F5刷新即登录。

![](./2022-08-16-00-34-44.png)


### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

