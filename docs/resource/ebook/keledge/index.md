---
title: "可知：多个知名出版社入驻的正版电子书平台，可试读、全文阅读"
description: "可知：多个知名出版社入驻的正版电子书平台，可试读、全文阅读"
slug: Keledge
date: 2022-08-09T23:20:53+08:00
draft: false
tags: ['电子书']
categories: ['']
---

## 简介

“可知”平台，是由电子工业出版社、人民邮电出版社、化学工业出版社、机械工业出版社、北京大学出版社等110家知名出版社入驻并直接提供数字资源的知识服务平台。

个人用户可试读，机构用户可通过“荐书”全文阅读。

图书质量非常高，不是扫描版，内容很多。

![](./2022-08-09-23-23-01.png)

### 相关链接

- [可知-国家知识服务平台可知分平台](https://redirect.1kbtool.com/?url=https://www.keledge.com/wrap/index)

<!-- 
[����н���һ�¿�֪��PDF�������� - �������ʴ�����  - �ᰮ�ƽ� - LCG - LSG |��׿�ƽ�|��������|www.52pojie.cn](https://www.52pojie.cn/thread-1099828-1-1.html)

[GitHub - ygcaicn/keledge at master](https://github.com/ygcaicn/keledge/tree/master)

部分书籍为 EPUB 或者 PDF 格式，可以考虑通过 JS 的方式将内容直接转化 DIV 为 PDF

https://github.com/eKoopmans/html2pdf.js

https://github.com/wkhtmltopdf/wkhtmltopdf

https://zhuanlan.zhihu.com/p/94608155
 -->
## 使用教程

个人用户可试读，机构用户可通过“荐书”全文阅读。大学生可以通过CARSI登录。

直接搜索阅读即可。

### 全文获取方式

机构用户通过荐书获取全文。

![](./2022-08-09-23-23-52.png)

荐书之后即可获得一天的阅读有效期。

![](./2022-08-09-23-51-11.png)

关于下载将在另外的文章中介绍。
## 附录

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

