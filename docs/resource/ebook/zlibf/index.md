---
title: "解决 Zlibrary 无法登录（卡死）问题"
slug: Zlibr
date: 2022-06-22T22:36:58+08:00
draft: false
tags: ['Z-Library','电子书']
categories: ['']
---

## 简介

目前一个很普遍的问题是，镜像站（不登陆）-提示下载额度用完，点击登录获取额度-跳转并成功登录-返回时找不到可用域名/卡死。

原因：

- 不登陆时，额度以IP计算。简单来说，通常你和你周围上网的人是共用一个IP，这意味着你们共用 5本/日 的额度。
- 登陆后，额度按账号计算。但点击登录后会转到官方登录界面，等你登录完成，他就会自动帮你寻找可用的**官方**网站——问题是官方网站全部无法访问——最后显示找不到域名/卡死。

解决方案：带着登录信息访问镜像网站，即可实现不跳转登录。

问题：盗号风险，请不要使用高价值账号。

终极解决方案：使用代理：[VPN 教程 ](https://redirect.1kbtool.com/?url=https://www.acrosswall.org/docs/subscribe/jms/Just%20My%20Socks) 。声明：与本站无关，本站不提供VPN服务！！！

## 使用教程

### 第1步：

访问Z-Library登录页面，Z-Library用户中心和主站是分开的，国内可以正常访问。

[https://zh.singlelogin.me/](https://pangniao.net/go/aHR0cHM6Ly96aC5zaW5nbGVsb2dpbi5tZS8)

### 第2步：

点击忘记密码，然后输入你注册Z-Library的邮箱，如下图：

![](2022-06-22-22-40-03.png)

![](2022-06-22-22-40-17.png)


### 第3步：

你的邮箱会收到一封名为“Z-Library: Password reset”的邮件。

把邮件中重置链接（下图圈出来的链接）复制到记事本（或者随便哪里，能编辑就行。）

链接举例：

```
https://1lib.domains/profileEdit.php?remix_userid=xxxxxxxxx&remix_userkey=xxxxxxxxxxxxxxxxxxxxxxx
```

![](2022-06-22-22-40-25.png)

### 第4步：

把链接中的域名修改为**可访问镜像域名**，也就是在[检测工具](https://check.1kbtool.com/zlibrary/)“民间镜像站点”中找一个绿色的网站，点进去，然后复制网址。

如：把`1lib.domains`修改为`zh.booksc.xyz`

举例如下：

原链接：

```
https://1lib.domains/profileEdit.php?remix_userid=xxxxxxxxx&remix_userkey=xxxxxxxxxxxxxxxxxxxxxxx
```

修改后链接：

```
https://zh.booksc.xyz/profileEdit.php?remix_userid=xxxxxxxxx&remix_userkey=xxxxxxxxxxxxxxxxxxxxxxx
```

### 第5步：

复制修改好后的链接，在浏览器中打开就OK了，打开页面默认是“用户信息页面”，点击“主页”回到首页，如下图：

![](2022-06-22-22-42-06.png)

##  

## 小编吐槽

@royse

属实麻烦，且不安全，建议 VPN。

## 附录

### Reference

1. [Getting Title at 38:11](https://pangniao.net/Z-Library-meiyouzhaodaokeyongdeyuming.html)

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

