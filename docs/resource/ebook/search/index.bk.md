---
title: "E搜-电子书聚合搜索"
slug: esou
date: 2022-05-24T14:50:55+08:00
draft: false
---

## 简介

在搜索电子书时我们时常要辗转多个网站进行搜索，效率低下。这里对常用的（而且技术上比较好做的）几个电子书搜索网站归总起来。聚合搜索。

![](2022-05-24-17-43-12.png)

### 相关链接

直达链接：[Esou-聚合电子书搜索](https://esou.1kbtool.com/)

[Z-Library：全球最大的数字图书馆/含打不开的解决方案/镜像](https://1kbtool.com/web/docs/resource/ebook/zlib/)

[电子书下载网站概览/大全](https://1kbtool.com/web/docs/resource/ebook/intro/)


## 使用教程

直接搜索即可。各引擎详细内容参见：[电子书下载网站概览/大全](https://1kbtool.com/web/docs/resource/ebook/intro/)

## 小编吐槽

@royse

花了两个小时搜了下实现原理，写了个草稿。没有做美化，算了。

## 附录

### Reference

1. [XmSearch 熊猫搜索-聚合文档搜索导航](https://xmsoushu.com/#/)
2. [不用爬虫，也能写一个聚合搜索引擎_优雅的程序酱的博客-CSDN博客](https://blog.csdn.net/hxx051/article/details/103881414)
3. [Quasar Framework - Build high-performance VueJS user interfaces in record time](https://quasar.dev/)
4. [Vue.js](https://v3.cn.vuejs.org/)
5. [GitHub - feige2/MoreSearch: 猫搜源代码。旨在聚合搜索引擎结果，提高搜索效率。](https://github.com/feige2/MoreSearch)

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

