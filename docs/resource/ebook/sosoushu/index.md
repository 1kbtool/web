---
title: "搜搜书-电子书聚合搜索下载网站"
description: "搜搜书-电子书聚合搜索下载网站"
slug: Sosoushu
date: 2022-07-22T11:15:51+08:00
draft: false
tags: ['电子书']
categories: ['']
---

## 简介

搜搜书是一个电子书聚合搜索下载网站，提供两种搜索方式。多网站支持。

手动切换版本，书源更多。

[搜搜书 多书源聚合搜索工具 Zlibrary 鸠摩](https://redirect.1kbtool.com/?url=https://www.sosoushu.com/)

![](2022-07-22-11-21-20.png)

自动搜索版本，一次搜索多个网站。

[搜搜书-聚合电子书搜索](https://redirect.1kbtool.com/?url=https://sosoushu.com/)

![](2022-07-22-11-22-32.png)

### 相关链接

- [搜搜书-聚合电子书搜索](https://sosoushu.com/)
- [搜搜书 多书源聚合搜索工具 Zlibrary 鸠摩](https://www.sosoushu.com/)

## 使用教程

开箱即用，直接搜索即可。

## 小编吐槽

@royse

挺好的一个聚合网站。

## 附录

### Reference

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

