---
title: "电子书网站精选"
date: 2022-02-27T13:16:15+08:00
draft: false
categories: ["网站分享"]
tags: ["资源网站","电子书"]
---

首先推荐几个常用的网站，都是我精选出来可以使用的。

相关链接：

[E搜-电子书聚合搜索](https://1kbtool.com/web/docs/resource/ebook/esou/)

[Z-Library：全球最大的数字图书馆/含打不开的解决方案/镜像](https://1kbtool.com/web/docs/resource/ebook/zlib/)

## Z-lib

[链接](https://redirect.1kbtool.com/?url=https://zh.z-lib.org/)

非常全。且支持发送到kindle、支持个人书架、收藏等。可找大学教材电子版。

![](2022-02-27-13-29-35.png)

![](2022-02-27-13-27-13.png)

## 淘链客

[链接](https://redirect.1kbtool.com/?url=https://www.toplinks.cc/s)

网盘集合型。国内资源大全。无广告直接下载。非常好用。

![](2022-05-24-17-59-15.png)

## 鸠摩搜索

[链接](https://redirect.1kbtool.com/?url=https://www.jiumodiary.com/)

属于网盘集合型，可找大学教材电子版。最近要关注公众号了。

![](2022-02-27-13-25-46.png)

## Lore Free

链接https://lorefree.com/

大量mobi、epub格式书籍。未登录状态下，每天有3个下载额度（包括电子书和论文），登录EOS账号以后再增加2个下载额度。

最近访问有些慢？

![](2022-02-27-13-37-25.png)

## 书格



中国数字古籍图书馆，各种古籍文献书法地图都有。

![](2022-02-27-13-33-54.png)

### 其他

### elib.cc （疑似失效）

[链接](https://elib.cc/)

无法注册，无法下载，不知是否失效。

![](2022-02-27-13-23-42.png)

一天只能下载三本书

### 雅书

[链接](https://redirect.1kbtool.com/?url=https://yabook.org/)

属于书籍推荐型。内容不多但是较精。偏文学、流行。书荒可以去这个网站。

可以无限次下载，只是下载较慢（跳诚通网盘）。

![](2022-02-27-13-21-27.png)

<!-- ## 电子书网站大全

除以上推荐的网站外，我们还收集了90多个网站。暂时没有一个个验证。如有失效请评论区提出。 -->

## 附录

### Reference

1. https://pangniao.net/z-library.html
2. https://pangniao.net/z-library-shouxian.html

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。
