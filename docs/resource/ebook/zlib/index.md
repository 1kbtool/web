---
title: "Z-Library：全球最大的数字图书馆/含打不开的解决方案/镜像"
date: 2022-05-23T12:35:07+08:00
draft: false
slug: zlib
categories: ["网站"]
tags: ["资源网站","电子书",'Z-Library']
---

## 简介

Z-Library 是成立于2009年的网站，免费提供书籍和文章下载，Z-Library 自称是全球最大的数字图书馆。对中英文出版物搜集比较好。但也存在部分教材/习题集/文档网盘可搜到但是Zlib没有的情况。

Z-library 官网一般不能直接访问。可以使用代理或访问镜像（见后）。

### 相关链接

官网：[Z-Library。世界上最大的数字图书馆。](https://redirect.1kbtool.com/?url=https://zh.z-lib.org/)

自动检测可用网站：[Zlibrary 可用检测](https://check.1kbtool.com/zlibrary/)

更多电子书网站可以参考：[电子书下载网站概览/大全](https://1kbtool.com/web/docs/resource/ebook/intro/)

一个聚合搜索工具：[E搜-电子书聚合搜索](https://1kbtool.com/web/docs/resource/ebook/esou/)

epub格式可以使用：[Epub在线阅读器](https://epub.1kbtool.com/)

## 使用教程

### 关于访问

目前国内直接访问官网时常失败，民间有很多公益镜像站点。推荐使用：[Zlibrary 可用检测](https://check.1kbtool.com/zlibrary/) 自动检测可用网站。

需要注意的是，镜像站**无法直接登录**，登录跳转时可能会卡死。另外，镜像站不登陆很容易触发下载限额。关于镜像站登录请参考：[解决 Zlibrary 卡死/找不到域名/达到限额问题](https://www.1kbtool.com/web/docs/resource/ebook/zlibr/)

以下列举一些镜像站：

https://www.bookzz.ren/

https://zh.booksc.eu/

https://zlib.shop/

https://zh.booksc.xyz/

https://zh.za1lib.vip/

https://zh.booksc.me/


### 关于下载

未注册每日限制下载5次，注册并验证邮箱后每日限制下载10次（只需要邮箱就可以注册），当然你也可以通过捐赠网站的方式获取更多下载次数和附加功能。如下载的文档是（第一次打开稍慢）

未登录时，下载限额按照IP计算。由于公用IP的存在，可能出现室友/隔壁等使用同一IP下载导致额度耗尽情况。**可以使用手机流量下载。**

<!-- #### 公益镜像

本站提供一个公益镜像，基于 Cloudfare Page Function。缺点：**此方法不支持登录下载！每日限额固定为5本/IP。**

[数字图书馆。搜索书籍。免费下载书籍](https://zlib.1kbtool.com/) -->
<!-- 
#### 登录跳转最新域名

虽然主站被限制访问，但 Z-library 的登录界面一直是可以直接连接的。您可以直接访问：

[Z-Library 登录](https://zh.singlelogin.me/)

注册/登录，**点击继续，就会自动跳转到当前地区可用的站点。**通过这个方法，也可以支持 >5 本下载。缺点：**可能出现 没有找到可用的域名 的提示！**

>说明：Z-Library登录页面会自动检测当前用户可访问地址，但如果这个被检测的“域名库”里面的地址全部失效，而其他国内可访问的新地址又没有被添加到“域名库”中的时候就会提示“没有找到可用的域名”。

#### 免登录/可用域名列表（持续更新）

免登录自动跳转：

[Z-Library single sign on](https://1lib.domains/?redirectUrl=/)

可用域名列表：

[数字图书馆。搜索书籍。免费下载书籍](https://zh.zlibrary.org/) 2022/5/23 -->

## 附录

### Reference

1. https://pangniao.net/z-library.html
2. https://pangniao.net/z-library-shouxian.html

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。




