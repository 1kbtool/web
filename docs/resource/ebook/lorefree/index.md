---
title: "Lorefree：高质量 mobi/epub 书籍，适合 Kindle 用户。文学类"
description: "Lorefree：高质量 mobi/epub 书籍，适合 Kindle 用户。文学类"
slug: Lorefree
date: 2022-08-11T09:36:18+08:00
draft: false
tags: ['']
categories: ['']
---

## 简介

**大量mobi、epub格式书籍**。未登录状态下，每天有3个下载额度（包括电子书和论文），登录EOS账号以后再增加2个下载额度。

非常适合 Kindle 用户使用。

![](./2022-08-11-09-39-01.png)

### 相关链接

- [LoreFree - 首个基于EOS和IPFS的去中心化知识共享社区](https://redirect.1kbtool.com/?url=https://lorefree.com/)

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

