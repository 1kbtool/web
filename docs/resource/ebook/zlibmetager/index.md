---
title: "使用 metager 间接访问 Zlibrary"
description: "使用 metager 间接访问 Zlibrary"
slug: Zlibmetager
date: 2022-08-15T22:07:38+08:00
draft: true
tags: ['Z-Library','电子书']
categories: ['']
---

## 简介

Zlibrary 官网，包括镜像最近都打不开。这里提供一个用 metager 匿名搜索引擎来间接访问的方式。

### 相关链接

- [MetaGer: Privacy Protected Search &amp; Find](https://redirect.1kbtool.com/?url=https://metager.org/)

## 使用教程


打开： [MetaGer: Privacy Protected Search &amp; Find](https://redirect.1kbtool.com/?url=https://metager.org/)

先搜索 zlibrary.org，然后选择 open anonymously

![](./2022-08-15-22-09-31.png)

![](./2022-08-15-22-09-42.png)

按照一般流程使用。页面内会有一段乱码，拉到下面，无视即可。

## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

