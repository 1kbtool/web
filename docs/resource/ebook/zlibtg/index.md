---
title: "1library bot ——稳定方便的 Zlibrary 下载机器人"
description: "1library bot——稳定方便的 Zlibrary 下载机器人"
slug: Tglibrary
date: 2022-07-23T22:40:26+08:00
draft: false
tags: ['电子书','Z-library']
categories: []
---

## 简介

1library 是 Zlibrary 官方推出的一个 Telegram（电报，国外社交软件）机器人，可以方便地下载书籍。免去网站失效烦恼。

![img](dbcc14d7d721d80e24261.png)

### 相关链接

- [Nekogram：开源给力的第三方 电报（Telegram） 客户端（自带代理）](https://www.1kbtool.com/android/docs/thirdparty/nekogram/)
- [Z-Library 登录 (singlelogin.me)](https://redirect.1kbtool.com/?url=https://zh.singlelogin.me/)
- Zlibrary 交流电报群：[zlibrary TG 交流群 点此直接加入](https://redirect.1kbtool.com/?url=https://t.me/zlibrarytg)

## 使用教程

首先需要有一个 Telegram 的客户端。这里推荐 Nekogram 客户端，自带代理功能。参考：[Nekogram：开源给力的第三方 电报（Telegram） 客户端（自带代理）](https://www.1kbtool.com/android/docs/thirdparty/nekogram/)

其次我们需要注册 Zlibrary 的账户：[Z-Library 登录 (singlelogin.me)](https://zh.singlelogin.me/) 。网站会根据你的节点所在地区自动分配账户所在地区。注册过程不赘述，注册后，**务必点击注册邮箱里的验证链接，完成邮箱验证，否则无法下载**。（已有 Zlibrary 账户的童鞋请直接跳过）

如图，登录 Zlibrary 账户后，点击页面右上角的“头像”，修改用户信息

![img](d6a6ca9aa14f9c27fa0a5.png)

![img](4e01805671326706f5a78.png)

在修改用户信息页面往下拉，在页面最底部找到并直接点击 “Link to Telegram bot”，自动跳转到 Telegram 客户端。

![img](c0ce6c8b4688dc8d75fbd.png)

![img](aaa7054f73c7de7b5ce4a.png)

点击 bot 的“START”完成绑定。直接在 bot 输入书籍的相关信息即可快速查找书籍，也可以直接输入 ISBN 编号。

![img](9d78f37fcb91c1933cfa8.png)




## 小编吐槽

@royse

配合 Nekogram 使用美滋滋。

## 附录

### Reference

- [绑定账户、使用 Telegram bot 下载 Zlibrary 上的电子书 – Telegraph](https://graph.org/Zlibrarybot-04-26-2)

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

