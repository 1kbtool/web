---
title: "高教书苑：高等教育出版社旗下电子书网站"
description: "高教书苑：高等教育出版社旗下电子书网站"
slug: Ebookhep
date: 2022-08-09T23:29:01+08:00
draft: false
tags: ['电子书']
categories: ['网站分享']
---

## 简介

高教书苑是高等教育出版社旗下的一个网站，里面有很多教材资源！

实际上高教书苑的资源都是来源于可知。**只能试读**！

![](./2022-08-09-23-32-45.png)

### 相关链接

[高教书苑](https://redirect.1kbtool.com/?url=https://ebook.hep.com.cn/ebooks/index.html#/)

<!-- ## 使用教程

一些个比较详细的教程 -->
<!-- 
## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法 -->

## 附录
<!-- 
### Reference -->

<!-- 参考的文章 1 -->

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

