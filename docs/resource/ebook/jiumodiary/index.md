---
title: "Jiumodiary"
description: "Jiumodiary"
slug: Jiumodiary
date: 2022-08-14T15:04:20+08:00
draft: true
tags: ['']
categories: ['']
---

## 简介

## 鸠摩搜索

[链接](https://www.jiumodiary.com/)

属于网盘集合型，可找大学教材电子版。最近要关注公众号了。

一个简短的介绍

### 相关链接

## 使用教程

一些个比较详细的教程

## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

