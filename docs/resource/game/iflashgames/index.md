---
title: "Iflash：经典 4399/Flash 小游戏在线玩，无需安装 FlashPlayer，基于 Ruffle"
description: "Iflash：经典 4399/Flash 小游戏在线玩，无需安装 FlashPlayer"
slug: Iflashgames
date: 2022-07-22T10:52:27+08:00
draft: false
tags: ['']
categories: ['']
---

## 简介

flash 淘汰，4399 上许多游戏需要老版浏览器才能玩。同时国内审核日趋严格，导致 4399 也要实名认证。

国外开源软件 Ruffle 已经给出了替代方案，虽然只支持部分功能。

[iFlashGames | 在线游玩经典 flash 小游戏](https://redirect.1kbtool.com/?url=https://play.iflash.games/) 是一个基于 Ruffle 的在线游玩小游戏的网站。目前游戏不多，但在扩充之中。您也可以提交游戏申请。

![](2022-07-22-11-10-25.png)


### 相关链接

- [iFlashGames | 在线游玩经典 flash 小游戏](https://redirect.1kbtool.com/?url=https://play.iflash.games/) 

## 使用教程

访问网站，开箱即玩。

## 小编吐槽

@royse

比较简陋的网站。

## 附录

### Reference

- [I Flash Games](https://redirect.1kbtool.com/?url=https://iflash.games/)

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

