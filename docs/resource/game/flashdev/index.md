---
title: "Flash Player 开发版：无广告的本地 Flash 模拟器，玩4399经典小游戏"
description: "Flash Player 开发版：无广告的本地 Flash 模拟器，玩4399经典小游戏"
slug: Flashdev
date: 2022-07-22T10:53:37+08:00
draft: false
tags: ['']
categories: ['']
---

## 简介

4399 上的小游戏都是基于 Flash 的，如果要玩就要下 Flash Player，不过百度搜出来的下载都是流氓软件很多广告。

Flash中国站点其实有Flash独立播放器，已确认此播放器不含弹窗广告和系统服务。

[Flash Player官方下载-Flash官网](https://redirect.1kbtool.com/?url=https://www.flash.cn/support/debug-downloads)


### 相关链接

## 使用教程


这只是一个播放器，需要另外下载游戏文件才能玩。以下列举了一些网站：


- [iFlashGames | 在线游玩经典 flash 小游戏](https://redirect.1kbtool.com/?url=https://play.iflash.games/)
- [Flash 保存计划](https://redirect.1kbtool.com/?url=https://flash.zczc.cz/) Ruffle 可下载 部分文件失效
- [Games Plus by CoolStreaming](https://redirect.1kbtool.com/?url=https://www.coolstreaming.us/arcade/) 英文 Ruffle 可通过 API 下载。实例： [Harum Scarum Free games online | CoolStreaming](https://redirect.1kbtool.com/?url=https://www.coolstreaming.us/forum/arcade/game/4651.html) 
- [Flash Games 1 Page - Flash Games Archive | FlashArch](https://redirect.1kbtool.com/?url=https://flasharch.com/en/archive) 英文

这些网站都可以下载游戏，为`swf`后缀。

下载完成后直接双击运行，左上角文件-打开-浏览-选择下载完成的 swf 文件，点击确定即可。

打开之后，点击 查看-显示全部，再对窗口最大化可以放大，或直接点击 查看-全屏。

## 小编吐槽

@royse

挺好用的。ruffle 模拟器有些不能跑。这个都行。

## 附录

### Reference

- [Flash 本地播放器纯净版本](https://redirect.1kbtool.com/?url=https://iflash.games/post/flash/)

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

