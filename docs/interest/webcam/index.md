---
title: "SkylineWebcams：全球景点高清直播"
date: 2022-02-27T20:27:28+08:00
draft: false
slug: Webcam
categories: ["网站分享"]
tags: ["趣站"]
---

最近发现的一个项目，还挺有意思的，这里分享给大家。使用时请注意法律法规。

SkylineWebcams可以让你看到全世界范围内部分景点高清网络摄像头的实时直播内容，让你足不出户就能游览全世界。

SkylineWebcams的摄像头资源目前覆盖了5大洲，56个国家/地区，上百个景点，包括不限于：城市风光、海滩、冲浪、码头、联合国教科文组织、风景、滑雪场、动物、火山、湖泊等内容。

[官网](https://redirect.1kbtool.com/?url=https://www.skylinewebcams.com/)

![](2022-02-27-20-30-43.png)

![](2022-02-27-20-31-48.png)

上班累了可以看下这个。

参考：

1. https://pangniao.net/SkylineWebcams.html