---
title: "【不太好用】Madiafire：10G免费网盘，邮箱注册"
date: 2022-02-05T19:34:53+08:00
draft: false
slug: Madiafire
categories: ["网盘相关"]
tags: ["网盘平台","文件分享"]
---

## 简介

国外的一款网盘软件，大小只有10G，**并且只需要邮箱就可以注册**。

国内访问速度时好时坏。仅供参考并不推荐。在国内使用的无需实名的网盘服务推荐使用[钛盘](https://soft.kermsite.com/p/tmp/)。

![](2022-02-05-19-38-28.png)

[下载测试](https://www.mediafire.com/file/qslb3ob1l181s8n/阿里云盘TV1.0.9.apk/file)

![](2022-02-05-19-40-19.png)

参考：
