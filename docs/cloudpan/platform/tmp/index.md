---
title: "钛盘：邮箱注册，不限速网盘，网页直接上传下载，无限容量"
date: 2022-02-09T20:50:57+08:00
draft: false
slug: Tmp
categories: ["网盘相关"]
tags: ["网盘平台","文件分享"]
---

[官网](https://app.tmp.link/)

![](2022-02-09-22-08-46.png)

钛盘是一款无限空间、不限速、无需客户端，免费的在线网盘服务，只需要邮箱即可注册，对在意个人信息的朋友比较友好。单文件支持10GB。

包括公共空间和私人空间两个计划，公共空间定时删除，无限容量。私有空间不会删除，5G容量。在上传文件时均可选择存储到私有/公共空间。

![](2022-02-09-22-02-13.png)

上传、下载速度都比较快。

[下载速度测试链接](http://tmp.link/f/6203aaa934b5d)

网站用户逻辑有些糟糕（可能是翻译的问题），上传的所有文件都可以点击主页的“流”进行查看。或者直接访问[此链接](https://app.tmp.link/?tmpui_page=/workspace)

![](2022-02-09-22-05-43.png)

参考：

1. https://www.iplaysoft.com/tmplink.html
2. https://www.appinn.com/tmp-link/
3. https://zhuanlan.zhihu.com/p/359193167