---
title: "Catbox"
date: 2022-02-22T16:20:30+08:00
draft: false
slug: Catbox
categories: ["实用工具"]
tags: ["网盘相关"]
---

## 简介

Catbox是一款国外的文件分享平台。由网友捐资维护。无需注册即可上传文件、生成分享链接。国内访问速度一般，视具体网络情况而定。网页二次元浓度较高。

[测试链接](https://files.catbox.moe/2pxrx9.zip)

不能上传`.exe, .scr, .cpl, .doc*, .jar`文件，但是可以上传压缩文件。

分为永久和临时。永久，文件最大为200M；临时，期限最长为3天，文件最大为1G。

[官网链接](https://redirect.1kbtool.com/?url=https://catbox.moe/)

[临时盘](https://redirect.1kbtool.com/?url=https://litterbox.catbox.moe/)

网站截图：

![](2022-02-22-16-24-42.png)

![](2022-02-22-16-25-04.png)

参考：
