---
title: "Pomf：一款开源的文件分享项目。并附大量基于本项目的文件分享服务，例如FileDitch等。"
date: 2022-02-22T16:32:41+08:00
draft: false
slug: Pomf
categories: ["网盘相关"]
tags: ["网盘平台","文件分享"]
---

Pomf是一款开源的文件分享项目，源码托管于GitHub。基于此源码许多网友搭建了自己的公益文件分享服务。最大支持到15G大文件。

[官网](https://pomf.se/)

[项目地址](https://github.com/Pomf/Pomf)

[使用本源码搭建的项目一览](https://status.uguu.se/clones.html)

![](2022-02-22-16-47-41.png)

用于测试的网站是FileDitch，本网站最大支持15G文件的上传分享。

![](2022-02-22-16-46-49.png)

[下载速度测试](https://bun.filedit.ch/mToCbfdtuxGSllpetqor.zip)




参考：
