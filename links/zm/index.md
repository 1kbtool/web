---
title: "Zm"
slug: Zm
date: 2022-05-25T23:17:22+08:00
draft: true
---

## 简介

[Forwarded from 链接收藏]
🖥 #网址 #字幕 #影音
YYeTs 人人影视
https://www.yysub.net/
R3字幕網
https://r3sub.com/
点点字幕
http://www.ddzimu.com/
字幕库(zimuku) -- 字幕下载网站
https://zmk.pw/
A4k字幕网
https://www.a4k.net/
SubHD
https://subhdtw.com/
射手网(伪) 
http://assrt.net/
字幕
https://www.podnapisi.net/
中文字幕网
http://cn.zimuzimu.com/
影视字幕
https://4k-m.com/zimu/
日夸字幕
https://www.rikua.com/
ZM字幕吧
http://www.zmbaa.com/


[Forwarded from 链接收藏]
🖥 #网址 #字幕-外文 #影音
Subtitles 
https://www.opensubtitles.org/en/search/subs
Moviesubtitles
http://www.moviesubtitles.org/
Subscene
https://subscene.com/
EZTV
https://eztv.re/
TVsubs.net
http://www.tvsubs.net/
Download English subtitles
https://english-subtitles.org/
MovieSubtitles
http://www.moviesubtitles.net/
DivX Movies
https://www.divxmoviesenglishsubtitles.com/
TVsubtitles
http://www.tvsubtitles.net/
Addic7ed
https://www.addic7ed.com/
YIFY
https://yts-subs.com/
Bollywood Movie Subtitles
https://www.bollynook.com
Subdl
https://subdl.com/subtitle.php
SubtitlesHub
https://subtitleshub.net/
Download Subs
https://subtitleseeker.in
### 相关链接

## 使用教程

一些个比较详细的教程

## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

