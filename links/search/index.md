---
title: "搜索引擎"
slug: Search
date: 2022-05-25T23:13:58+08:00
draft: true
---

## 简介




### TG 搜搜


感谢大佬收录，现在频道可以直接搜了。
推荐直接私聊机器人 @sssoou_bot 即可

机器人搜索
@sssoou_bot
群组搜索
@sssoou_resource
网页搜索
http://www.sssoou.com/

### [Forwarded from 链接收藏]
🖥 #网址
#搜索引擎 2
华为搜索引擎 Petal Search
https://petalsearch.com/

Internet Archive，查找书籍、文献、杂志等文件的搜索引擎
http://archive.org/
LookAhead搜索引擎。它主要用于查找新闻、政策和研究报告。
http://lookahead.surfwax.com/
ItemFix - 深入视频搜索引擎
https://www.itemfix.com/
http://www.liveleak.com/


20220415 #更新
1024搜-程序员专属的搜索引擎
https://www.1024sou.com/
逗比拯救世界--专业的表情包搜索网站
https://www.dbbqb.com/
#更新
汤姆聚合搜索
https://www.tomfind.com/

You
You.com

### 相关链接

## 使用教程

一些个比较详细的教程

## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

