---
title: "Plugs"
slug: Plugs
date: 2022-05-25T23:09:20+08:00
draft: true
---

## 简介

没有插件的浏览器是不完整的。

### 相关链接

https://chrome.google.com/webstore/detail/linkclump/lfpjkncokllnfokkgpkobnkbkmelfefj

链接躲开

[Forwarded from 链接收藏]
🖥 #网址
#脚本分享
🔥🔥🔥文本选中复制🔥🔥🔥
https://greasyfork.org/zh-CN/scripts/405130-文本选中复制
豆瓣资源下载大师：1秒搞定豆瓣电影|音乐|图书下载
https://greasyfork.org/zh-CN/scripts/329484-豆瓣资源下载大师-1秒搞定豆瓣电影-音乐-图书下载
GM scripts for Firefox and Chrome | GM
https://lkytal.github.io/GM/
Telegraph 拖拽传图
https://greasyfork.org/zh-CN/scripts/387966-telegra-ph-拖拽传图
V2EX Pro
https://greasyfork.org/zh-CN/scripts/397787-v2ex-pro
gaodingWatermark
https://greasyfork.org/zh-CN/scripts/441367-gaodingwatermark
csdn
https://greasyfork.org/zh-CN/scripts/372452-csdn自动展开-去广告-净化剪贴板-免登陆
GitHub - the1812/Bilibili-Evolved: 强大的哔哩哔哩增强脚本
https://github.com/the1812/Bilibili-Evolved
破解微信编辑器会员限制
https://greasyfork.org/zh-CN/scripts/392046-破解微信编辑器会员限制
阿里云盘网页优化-文件名防遮挡 / 进度条隐藏 / 播放窗口防遮挡 / 去广告
https://greasyfork.org/zh-CN/scripts/424170-阿里云盘网页优化-文件名防遮挡-进度条隐藏-播放窗口防遮挡-去广告
阿里云盘 突破视频2分钟限制等
https://greasyfork.org/zh-CN/scripts/425955-阿里云盘
GitHub - XIU2/UserScript: 🐵 自用的一些乱七八糟 油猴脚本~
https://github.com/XIU2/UserScript
浏览器脚本 soTab：搜索引擎间互相快速跳转 | 中原驿站
https://hzy.pw/p/1849
解锁B站大会员番剧、B站视频解析下载；全网VIP视频免费破解去广告；全网音乐直接下载；油管、Facebook等国外视频解析下载；网盘搜索引擎破解无限下载等
https://greasyfork.org/zh-CN/scripts/418804-解锁b站大会员番剧-b站视频解析下载-全网vip视频免费破解去广告-全网音乐直接下载-油管-facebook等国外视频解析下载-网盘搜索引擎破解无限下载等
秒传链接提取
https://greasyfork.org/zh-CN/scripts/424574-秒传链接提取
自动无缝翻页
https://greasyfork.org/zh-CN/scripts/419215-自动无缝翻页

网页翻译 ☆（推荐）
https://greasyfork.org/zh-CN/scripts/398746-网页翻译
网页翻译助手
https://greasyfork.org/zh-CN/scripts/389784-网页翻译助手

20220505 #更新
open the link directly 去除中转链接
https://greasyfork.org/zh-CN/scripts/442187-open-the-link-directly


Tampermonkey  脚本管理器

https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo

暴力猴 脚本管理器

https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag

TabCopy 标签页复制

https://chrome.google.com/webstore/detail/tabcopy/micdllihgoppmejpecmkilggmaagfdmb

极速二维码-多场景解码工具

https://chrome.google.com/webstore/detail/the-quick-qr-code-multi-s/eefoinodjigoedmfjkbikmdhecggljgc

Extension Manager 插件管理器

https://chrome.google.com/webstore/detail/extension-manager/bfmjiakdfoacijccjgkkhoepjklhohle

Adblock Plus - 免费的广告拦截器

https://chrome.google.com/webstore/detail/adblock-plus-free-ad-bloc/cfhdojbkjhnklbpkdaibdccddilifddb

AdGuard 广告拦截器

https://chrome.google.com/webstore/detail/adguard-adblocker/bgnkhhnnamicmpeenaelnjfhikgbkllg

Base64 encode/decode selected text  Base64 编码解码

https://chrome.google.com/webstore/detail/base64-encodedecode-selec/gkdcpimagggbnjdkjhbnilfeiidhdhcl

crxMouse Chrome™ 手势

https://chrome.google.com/webstore/detail/crxmouse-chrome-gestures/jlgkpaicikihijadgifklkbpdajbkhjo

Save Page WE  页面保存

https://chrome.google.com/webstore/detail/save-page-we/dhhpefjklgkmgeafimnjhojgjamoafof

Vimium 浏览器快捷键

https://chrome.google.com/webstore/detail/vimium/dbepggeogbaibhgnhhndojpepiihcmeb



20220317 #更新

OneTab - Chrome 网上应用店

https://chrome.google.com/webstore/detail/onetab/chphlpgkkbolifaimnlloiipkdnihall



Relingo - Master words from any webpage - Chrome 网上应用店

https://chrome.google.com/webstore/detail/relingo-master-words-from/dpphkcfmnbkdpmgneljgdhfnccnhmfig



Linkclump - Chrome Web Store: https://chrome.google.com/webstore/detail/linkclump/lfpjkncokllnfokkgpkobnkbkmelfefj



Search-from-Popup-or-ContextMenu

https://github.com/YoshifumiFuyuno/Search-from-Popup-or-ContextMenu



草料二维码-快速生码和解码工具

https://chrome.google.com/webstore/detail/草料二维码-快速生码和解码工具/moombeodfomdpjnpocobemoiaemednkg



小舒同学 - 基于书签的新建标签页

https://chrome.google.com/webstore/detail/小舒同学-基于书签的新建标签页/eldcinofoklpfhaanlhmkmadehfgcnon



可可翻译 - Chrome 网上应用店

https://chrome.google.com/webstore/detail/sctranslator/icfnljfpacimpcbpammmbclmhenimhfc


## 使用教程

一些个比较详细的教程

## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

