---
title: "在线查毒工具 VirusTotal"
slug: Virustotal
date: 2022-05-31T20:15:25+08:00
draft: true
---

## 简介

在线查毒工具 VirusTotal

[一个简短的介绍](https://telegra.ph/%E5%9C%A8%E7%BA%BF%E6%9F%A5%E6%AF%92%E5%B7%A5%E5%85%B7-VirusTotal-%E7%9A%84-N-%E7%A7%8D%E7%8E%A9%E6%B3%95-03-28)

课代表：
1查毒的技术远离
2功能介绍：压缩包、exe程序查毒
3功能介绍：网站查毒

360、电脑管家之类的可以卸载，可尝试去使用火绒盾小巧精悍，但不得不说这工具对小白来说有点鸡肋。

### 相关链接

## 使用教程

一些个比较详细的教程

## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

