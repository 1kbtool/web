---
title: "404"
slug: 404
date: 2022-05-31T20:20:07+08:00
draft: true
---

## 简介

[Forwarded from Dejavu's Blog]
[ Photo ]
#趣站 #HTTP #Status

▌http\.dog

使用狗狗图表示的 HTTP 状态码 | Via #33398

 (https://t.me/DejavuBlog/1753?comment=33398)Webiste: https://http.dog/

频道 @DejavuBlog
群组 @DejavuGroup


[Forwarded from Dejavu's Blog]
[ Photo ]
#GitHub #发现 #趣站 #HTTP #Status

▌http\.cat

使用猫猫图表示的 HTTP 状态码，支持 #API

GitHub (https://github.com/httpcats/http.cat) | Website

 (https://http.cat/)频道 @DejavuBlog
群组 @DejavuGroup

https://http.cat/
### 相关链接

## 使用教程

一些个比较详细的教程

## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

